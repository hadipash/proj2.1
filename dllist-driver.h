#include "dllist.h"

void generate(DLList *list, int n); // generate N elements with random values
void remove(DLList *list, int n);   // remove N elements starting from the head of the list
