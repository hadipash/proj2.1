#include <iostream>
#include "dllist-driver.h"

using namespace std;

int main() {
	DLList *list = new DLList;
	int n;

	// read the number of elements from the keyboard
	cout << "Enter number of elements in the list: ";
	cin >> n;

	// generate a list
	generate(list, n);

	// print out generated elements on the screen
	DLLElement *ptr = list->GetFirst();
	for (int i = 0; i < n && ptr; i++) {
		cout << ptr->key << " ";
		ptr = ptr->next;
	}
	cout << endl;

	// remove all elements form the list
	remove(list, n);
	delete list;

	return 0;
}
