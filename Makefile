CC=g++
SOURCES=dllist.cc dllist-driver.cc dllist_test.cc

OBJECTS=$(SOURCES:.cc=.o)
EXECUTABLE=dllist_test

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@

.c.o:
	$(CC) -c $< -o $@

clean:
	rm -rf $(OBJECTS) $(EXECUTABLE)
