#include <cstdlib>
#include <iostream>
#include <stdlib.h>
#include <ctime>
#include "dllist-driver.h"

using namespace std;

void generate(DLList *list, int n) {
	srand(time(0));

	// generate N items and insert them into the list
	for (int i = 0; i < n; i++) {
		void *item = NULL;
		item = (void*)(rand() % 100);
		list->SortedInsert(item, (int)item);
	}
}

void remove(DLList *list, int n) {
	// remove N elements or until the list has no more elements
	for (int i = 0; i < n && !list->IsEmpty(); i++) {
		void *item = NULL;
		int *key = new int;

		item = list->Remove(key);
		cout << "Item " << *key << " has been removed from the list" << endl;

		delete key;
	}
}
